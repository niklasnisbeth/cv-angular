Niklas Nisbeth's CV (Angular)
====

This is my cv, in the form of an Angular app, using the angular-material components.

# What is going on?

The contents is simply a Javascript object (see the datafetch service, basically pre-parsed JSON). It's basically a list of 'sections'.

Each section is rendered as a tab. There are three types of section that are each rendered by a custom component.

One custom component renders lists of expansion panels. Another displays a single blurb as a card with an image (the about page), another displays a list of cards. The last two could probably be simplified.

# Shortcomings

* Lack of tests.
* Lack of translations
* The styling/theming is all in one file
* It looks bland and tasteless which is just totally not who am I.
* Some of the component names were changed without changing filenames. The names aren't always very good.
* The layout is not fully responsive and will maybe look a little weird on small screens in portrait mode.
* The component matching each section type has to be added to the main HTML (app.component.html) and is selected via *ngIF. The components only differ in their templates, but I could not find a way to dynamically select templates for Angular components, which would've simplified things.
* The data explicitly gives the type of each section. There should be a way around this, by simply trying to parse each section of the 'JSON' into the Typescript domain objects.
