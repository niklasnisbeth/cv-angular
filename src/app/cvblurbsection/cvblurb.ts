export class CvBlurb {
  headline: string;
  image: string;
  subtitle: string;
  content: string[];
  links: object[];
}
