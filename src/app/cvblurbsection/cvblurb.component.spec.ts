import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CvblurbComponent } from './cvblurb.component';

describe('CvblurbComponent', () => {
  let component: CvblurbComponent;
  let fixture: ComponentFixture<CvblurbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CvblurbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CvblurbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
