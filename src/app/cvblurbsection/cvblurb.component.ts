import { Component, OnInit, Input } from '@angular/core';

import {MatCardModule} from '@angular/material/card';

import { CvBlurb } from './cvblurb';

@Component({
  selector: 'app-cvblurbsection',
  templateUrl: './cvblurb.component.html',
  styleUrls: ['./cvblurb.component.css']
})
export class CvblurbComponent implements OnInit {
  @Input() contents: CvBlurb;

  constructor() { }

  ngOnInit() {
  }

}
