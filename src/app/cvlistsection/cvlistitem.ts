export class CvListItem {
  time: string;
  name: string;
  place: string;
  desc: string[];
  keywords: string;

  static fromJSON(json: object): CvListItem {
    console.log("hello from json");
    return Object.create(CvListItem.prototype);
  }
}
