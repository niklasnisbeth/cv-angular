import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CvitemsComponent } from './cvitems.component';

describe('CvitemsComponent', () => {
  let component: CvitemsComponent;
  let fixture: ComponentFixture<CvitemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CvitemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CvitemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
