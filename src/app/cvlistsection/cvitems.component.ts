import { Component, OnInit, Input } from '@angular/core';
import { CvList } from './cvlist';

@Component({
  selector: 'app-cvlistsection',
  templateUrl: './cvitems.component.html',
  styleUrls: ['./cvitems.component.css']
})
export class CvitemsComponent {
  constructor() {
  }

  @Input() contents: CvList;
}
