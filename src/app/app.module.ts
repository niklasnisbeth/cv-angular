import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CvitemsComponent } from './cvlistsection/cvitems.component';
import { CvblurbComponent } from './cvblurbsection/cvblurb.component';
import { DatafetchService } from './datafetch.service';

import { MatExpansionModule, MatCardModule, MatTabsModule, MatGridListModule } from '@angular/material';
import { CardlistComponent } from './cardlist/cardlist.component';

@NgModule({
  declarations: [
    AppComponent,
    CvitemsComponent,
    CvblurbComponent,
    CardlistComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NoopAnimationsModule,
    MatTabsModule,
    MatCardModule,
    MatExpansionModule,
    MatGridListModule
  ],
  providers: [ DatafetchService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
