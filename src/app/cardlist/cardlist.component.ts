import { Component, OnInit, Input } from '@angular/core';

import {MatCardModule} from '@angular/material/card';

import { Card } from './cardlist';

type Cards = Card[];

@Component({
  selector: 'app-cardlist',
  templateUrl: './cardlist.component.html',
  styleUrls: ['./cardlist.component.css']
})
export class CardlistComponent implements OnInit {

  @Input() contents: { items: Cards };

  constructor() { }

  ngOnInit() {
  }
}
