import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { CvSection } from './cvsection';

@Injectable()
export class DatafetchService {
  private data: CvSection[] = [{
    'key': 'About',
    'type': 'blurb',
    'contents': {
      headline: 'Niklas Nisbeth',
      subtitle: 'Programmer, Linux whisperer, all-round geek',
      content: ['Hello! I\'m Niklas. I\'m among other things a programmer, living in Copenhagen, Denmark.', 'This is my cv, formatted by an application I wrote in Typescript using Angular 5 and the Material components. The source code is available on Gitlab.', 'Click the tabs above for more info, or find me across the web.'],
      links: [
        {
          'text': 'This project on GitLab',
          'url': 'https://gitlab.com/niklasnisbeth/cv-angular',
        },
        {
          'text': 'My other things on Gitlab',
          'url': 'https://gitlab.com/users/niklasnisbeth',
        },
        {
          'text': 'My project on Github',
          'url': 'https://github.com/niklasnisbeth',
        },
        {
          'text': 'Me on Twitter',
          'url': 'https://twitter.com/niklasnisbeth',
        },
        {
          'text': 'Me on Keybase',
          'url': 'https://keybase.io/niklasnisbeth'
        }
      ],
      image: ''
    }
  },
  {
    'key': 'Skills',
    'type': 'cardlist',
    'contents': { items: [
      {
        'key': 'Code',
        'text': "Programming in functional, imperative and object-oriented styles in a range of languages, across several platforms. I've written non-trivial things in Scala, Java, C, Scheme, Lua and Javascript, and I've worked with several others like Python, Ruby, OCaml and Rust."
      },
      {
        'key': 'Web',
        'text': "Web development, both front and backend. I have built REST APIs in several stacks, and functional faces for them using plain HTML, CSS, and JS, or application frameworks like Angular or React."
      },
      {
        'key': 'Linux',
        'text': "Linux, both as a user and as an administrator. I've lived on the command line since 2005. Shell scripts, makefiles, systemd units, Docker are all no problem. I'm also familiar with setting up embedded, non-x86 systems using buildroot and OpenWRT."
      },
      {
        'key': 'Cloud',
        'text': "AWS: I've worked with EC2, and ECS on autoscaling groups. Also S3, as well as some understanding of IAM roles and policies."
      }
    ]}
  },
    {
      'key': 'Recent employment history',
      'type': 'list',
      'contents': {items: [
        {
          'name': 'Automation & testing',
          'place': 'Realm',
          'time': 'May 2017--',
          'keywords': 'Linux, Docker, shell, Javascript/Typescript, ansible, AWS',
          'desc': ['I helped design and implement a modular client (NodeJS) for load testing a server product. We used AWS\'s ECS and Cloudformation to run up to 10,000 instances of this client, against servers on Packet.net machines. Automated setup, teardown using Jenkins/Shell/Ansible, and collection of analytics to InfluxDB/Grafana. I\'m currently migrating some of this into Kubernetes.',
          'Additionally, I\'m one half of the automation/CI team, supporting a team of 20+ developers in Copenhagen and the USA. We use Jenkins and a lot of Groovy, bash, makefiles and Docker containers to build and distribute a core library written in C++, as well as the bindings for several languages, platforms and architectures. The infrastructure is a mix of on-site hardware and other people\'s computers, including AWS.']
        },
        {
          'name': 'Funemployed',
          'place': '',
          'time': 'March 2016--May 2017',
          'keywords': 'C, Verilog, electronics, fun',
          'desc': ['Took some time out to play with some embedded/electronics projects and make some music. Spent a lot of time in Labitat, learned about FPGAs, wrote C, hand-built a couple of mechanical keyboards, and designed and prototyped a sampler/drum machine that I\'ll finish and release as an open source/DIY project One Of These Days...', 
            'Also did some short term/freelance web work (some more Scala at Collapio, a PHP/Symfony project, some Elixir/Phoenix).'],
        },
        {
          'name': 'Developer and ops-topus',
          'place': 'Nota',
          'time': 'October 2015--March 2016',
          'keywords': 'Docker, Linux, nginx, shell, PHP, MySQL, CSS',
          'desc': ['Helped implement and launch a website based on a popular open source CMS written in PHP.',
            'Worked mostly on devops: Docker-containers, server configuration, general Linux-fiddling regarding backup and deploy mechanisms, etc.',
            'In addition to this I worked with the full text search framework Solr, as well as some styling (Sass) under the guidance of a graphic designer.',
            'Left because I didn\'t want to spend more time with that CMS.']
        },
        {
          'name': 'Full-stack Developer',
          'place': 'Collappio',
          'time': 'May 2014--October 2015',
          'keywords': 'Scala/Play, Javascript, Angular, CSS, full stack',
          'desc': ['I was hired to design and implement the backend for a communications platform/social medium that was a skunkworks project at a Danish telco. I worked primarily in Scala using the Play framework. The backend consisted of a REST API for client initiated communications, and a realtime API passing JSON over PubNub for server initiated communications.',
            'The backend involved integration with various third party services: Hosted Redis, Amazon S3, Amazon Lambda, TokBox (WebRTC/video streaming), Mailgun, Google APIs. Hosted at Heroku.',
            'In addition to the Android client that was developed, I designed and implemented a web client for this API, a single-page app written in AngularJS, ExpressJS, NodeJS, Less CSS.',
            'The project was never launched, but the prototype was fully working, including text, audio and video chat for multiple parties, file uploads, calendar functionality for planning meetings, and other things.'
          ]
        }
      ]}
    }, {
      'key': 'Education, etc.',
      'type': 'list',
      'contents': { items: [
        {
          'name': 'General-purpose hacking',
          'place': 'Labitat',
          'time': '2009--',
          'keywords': 'self-directed learning',
          'desc': [
            'Labitat is a hackerspace/makerspace/basement-full-of-nerds in Copenhagen. I\'ve been a regular since it got started in 2009. I\'ve made some great friends and learned lot about electronics and embedded code, how to solder and to work wood and metal both by hand and by machine. I\'m currently the chairman, formally, but it\'s mostly run by consensus at bi-weekly meetings.'
          ]
        },
        {
          'name': 'MA, General linguistics',
          'place': 'University of Copenhagen',
          'time': '2013',
          'keywords': 'language, cognition, computational linguistics, programming',
          'desc': ['A (nominally) two-year course that covers different aspects of language and language use.',
            'Master\'s Thesis (60 pages) on multi-word expressions in machine-readable semantic resources, with a focus on the interface between semantics and syntax.',
            'Electives in Computer science & Cognition, including courses in programming and data structures and algorithms, basic machine learning, etc.']
        },
        {
          'name': 'BA, Iranian studies',
          'place': 'University of Copenhagen',
          'time': '2009',
          'keywords': 'middle eastern language, history, culture',
          'desc': ['A four-year course on all aspects of Iranian history and culture, as well as modern Persian language.', 'I did optional courses in general linguistics and Arabic language.']
        }
      ]}
    },
  ];

  constructor() {
  }

  getSections(): Observable<CvSection[]> {
    return of(this.data);
  }
}
