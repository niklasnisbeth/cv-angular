import { Component } from '@angular/core';
import { DatafetchService } from './datafetch.service';
import { CvSection } from './cvsection';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Niklas Nisbeth\'s cv';

  sections: CvSection[];

  constructor(private data: DatafetchService) {
  }

  getSections() {
    this.data.getSections()
      .subscribe(sections => this.sections = sections);
  }

  // hack around a bug in angular-material
  // https://github.com/angular/material2/issues/5269#issuecomment-346166641
  selectedTabIndex = 0;
  tabChange(event) {
    Promise.resolve().then(() => this.selectedTabIndex = event.index);
  }

  ngOnInit() {
    this.getSections();
  }
}
