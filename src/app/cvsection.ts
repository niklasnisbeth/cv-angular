export interface CvSection {
  key: string;
  type: string;
  contents: object;
}
